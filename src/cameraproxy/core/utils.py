import logging
import time

def make_logger(name, /, level=logging.INFO):
        """
        helper function to initialize loggers for various modules
        """
        logger = logging.getLogger(name)
        handler = logging.StreamHandler()
        formatter = logging.Formatter(
                '[%(asctime)s][%(name)-12s][%(levelname)-8s]:: %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        logger.setLevel(level)
        return logger

class Throttle:
    def __init__(self, interval=0.01) -> None:
         self._interval = interval

    def __enter__(self):
        self.__start = time.time()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        time_to_sleep = time.time()-self.__start - self._interval
        if time_to_sleep > 0:
                time.sleep(time_to_sleep)