from threading import Thread, Lock
from queue import Empty
from .messagequeue import MessageQueue
from .jpeg import validate, quick_check
from .utils import make_logger

class JPEGCache:
    def __init__(self, mq:MessageQueue) -> None:
        self._log = make_logger(__name__)
        self._queue = mq.subscriber()
        self._thread = Thread(target=self._loop)
        self._get_timeout = 1.0
        self.__lock = Lock()
        self.__last_frame = None
        self.__last_valid_frame = None
        pass 

    def start(self):
        if self._thread.is_alive():
            self._log.warning(f"Thread already started!")
        else:
            self._thread.start()

    def _loop(self):
        while True:
            try:
                self.last_frame = self._queue.get(self._get_timeout)
            except Empty:
                pass # happens on timeout, it is OK

    @property 
    def last_valid_frame(self):
        with self.__lock:
            if self.__last_valid_frame is None:
                return None
            return self.__last_valid_frame.copy()

    @property 
    def last_frame(self):
        with self.__lock:
            if self.__last_frame is None:
                return None
            return self.__last_frame.copy()

    @last_frame.setter
    def last_frame(self,new_frame):
        with self.__lock:
            self.__last_frame = new_frame
        if validate(new_frame):
            with self.__lock:
                self.__last_valid_frame = new_frame
        else:
            print("GOT INVALID FRAME")