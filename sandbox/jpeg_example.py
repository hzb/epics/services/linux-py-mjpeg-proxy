from struct import unpack
import sys

marker_mapping = {
    0xffd8: "Start of Image",
    0xffe0: "Application Default Header",
    0xffdb: "Quantization Table",
    0xffc0: "Start of Frame",
    0xffc4: "Define Huffman Table",
    0xffda: "Start of Scan",
    0xffd9: "End of Image"
}


class JPEG:
    def __init__(self, image_file):
        with open(image_file, 'rb') as f:
            self.img_data = f.read()

    def RemoveFF00(self,data):
        """
        Removes 0x00 after 0xff in the image scan section of JPEG
        """
        datapro = []
        i = 0
        while True:
            b, bnext = unpack("BB", data[i : i + 2])
            if b == 0xFF:
                if bnext != 0:
                    break
                datapro.append(data[i])
                i += 2
            else:
                datapro.append(data[i])
                i += 1
        return datapro, i



    def StartOfScan(self, data, hdrlen):
        data, lenchunk = self.RemoveFF00(data[hdrlen:])
        return lenchunk + hdrlen
    
    def decode(self):
        data = self.img_data
        while True:
            (marker,) = unpack(">H", data[0:2])
            print(marker_mapping.get(marker))
            if marker == 0xFFD8:
                data = data[2:]
            elif marker == 0xFFD9:
                return
            else:
                (len_chunk,) = unpack(">H", data[2:4])
                len_chunk += 2
                chunk = data[4:len_chunk]
                if marker == 0xFFC4:
                    #self.decodeHuffman(chunk)
                    pass
                elif marker == 0xFFDB:
                    pass
                    #self.DefineQuantizationTables(chunk)
                elif marker == 0xFFC0:
                    #self.BaselineDCT(chunk)
                    pass
                elif marker == 0xFFDA:
                    len_chunk = self.StartOfScan(data, len_chunk)
                    pass
                else:
                    raise Exception(f"Unsupported marker: 0x{marker:02x}")
                data = data[len_chunk:]
            if len(data) == 0:
                break
if __name__ == "__main__":
    #print('Number of arguments:', len(sys.argv), 'arguments.')
    #print('Argument List:', str(sys.argv))

    if len(sys.argv)==1:
        print(f"USAGE: python {sys.argv[0]} JPEG_FILENAME [JPEG_FILENAME [JPEG_FILENAME] [...]]")

    for filename in sys.argv[1:]:
        JPEG(filename).decode()
