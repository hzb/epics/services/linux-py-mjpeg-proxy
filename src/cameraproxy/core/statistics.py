
from array import array
import time
import threading

NaN = float('nan')

class FrameStats:
    def __init__(self,average=60) -> None:
        self._points = average
        self._mutex = threading.Lock()
        self.reset()

    def reset(self):
        #self.__buffer = array('d', [NaN for i in range(0,self._points)])
        self.__buffer = array('d', [NaN for i in range(0,self._points)])
        #self.__buffer = [NaN for i in range(0,self._points)]
        self.__index = self._points-1

    def next(self):
        with self._mutex:
            self.__index=(self.__index+1)%self._points
            self.__buffer[self.__index]=time.time()

    def fps(self):
        #index_last=(self.__index)%self._points
        with self._mutex:
            index = self.__index
            index_next = (index+1)%self._points
            last=self.__buffer[index]
            previous = self.__buffer[index_next]
            try:
                return self._points/(last-previous)
            except:
                return NaN#f"index:{self.self.__index}::{last}::{previous}"
