# Camera proxy server written in python

Using multiple network cameras for the experimental controls presented multiple problems. Among them was:
* Usual network cameras does not cope well with multiple clients. After 2-3 lagging starts or camera can hang
* Occasionally a camera must be replaced or migrated to a new host. Reconfiguration of multiple clients can be problematic
* Not all network cameras properly follow standards which creates problem with some clients 
* EPICS URL camera client is sensitive to errors in JPEG decoding. Some kind of validated image is needed. If the bad image EPIC URL camera client just stops acquisition which can be problematic for software using that camera
* Camera image sometimes need to be pre-processed (for example rescaled or intensity normalized). But it needs to be done 'per-client' basis, not for all clients at once. This functionality is missing for most of the network cameras.

To solve this issues I developed mjpeg camera proxy using Node.js few years ago. And while it worked OK(ish) there was some problems and bugs which I never had time to fix. This mostly due to the fact that was the only project I ever written in Node.js and I have extremely limited experience with it.
After recent updates the old code was mostly broken and I decided to rewrite it completely, now in python. As python nowadays is one of the primally languages used for the controls hopefully this project will be developed and supported more actively.

## Installation

Clone this repo to a chosen path and then run the installation process.

Installation is a two step process.
First one need to install system-wide dependencies:

```
## turbo jpeg lib and headers
sudo apt install libturbojpeg0  libturbojpeg0-dev
```

And then create and populate a virtual environment:
```
pip -m venv .venv
. .venv/bin/activate
pip install cffi # Needs to be installed ahead due to a bug in jpegtran-cffi package
pip install -e .
```
## Running the app

After creating `cameras.json` configuration file (see [Configuration](##Configuration) section below) just run `cameraproxy` command:
```sh
(.venv) ovsyannikov@rovm-asrv02:~/projects/py-camera-proxy$ cameraproxy 
```

## Configuration

Configuration is a JSON file containing dictionary of the camera descriptions, single camera is:

```json
    "pm4wau": {
            "description": "PM4 WAU view",
            "url": "http://cam1-pgm1s8.exp.bessy.de/ipcam/mjpeg.cgi",
            "username": "user",
            "password": "field",
            "enabled": true
    },
```

|  parameter  |    default    | available | description  |
| ----------- | ------------- | --------  | -----------  |
| description | _CAMERA_NAME_ | R0-1      | description of the camera |
| url         | _None_        | R0-1      | url of external camera mjpeg stream |
| username    | _None_        | R0-1      | user name for external camera if it requires authentication  |
| password    | _None_        | R0-1      | user name for external camera if it requires authentication  |
| enabled     | True          | R0-1      | Allows to disable camera from loading/connecting |


## API

For every configured camera generally URL is formed like this:
### https://yourserver.tld/CAMERA_NAME/client.json

This url returns json file with information about remote camera connection status and some statistics

| field          | Available from |  Info  |
| -------------- | -------------- | ------ |
| connected      | R0-5           | bool, connection status |
| fails          | R0-5           | int, number of failed connection attempts |
| reconnects     | R0-5           | int, number of successful connection attempts |
| url            | R0-5           | str, url of the remote camera |
| fps            | R0-5           | float, frames per second from the remote camera |

Here is an example of possible response:
```json
{"connected":false,"fails":279,"fps":NaN,"reconnects":0,"url":"http://ldp-hwctl02.exp.helmholtz-berlin.de:20004/SAMPLE.MJPG.mjpg"}
```

### https://yourserver.tld/CAMERA_NAME/TYPE.EXT

EXT field can be ".jpeg", ".jpg", ".mjpeg" and ".mjpg". They will produce accordingly either still image or MJPEG streams
TYPE part of the name can the following:

| TYPE           | Available from |  Info  |
| -------------- | -------------- | ------ |
| last           | R0-5           | last known image |
| valid          | R0-5           | last known valid image |
| next           | R0-5           | wait and get next image from the source |
| raw            | R0-5           | try to get new image, on timeout return last known one |
| rpi06          | R0-5           | WORKAROUND: special downsized image for the rpi06 host. will be removed once processing pipeline will be implemented  |
| autocontrast   | R0-5           | TEST: testing autocontrast  |
| test           | R0-5           | TEST: just some testing |

Here are some examples of urls:
```
http://rovm-asrv02.exp.helmholtz-berlin.de:9090/ldp-side/valid.jpeg
http://rovm-asrv02.exp.helmholtz-berlin.de:9090/pm4mono/next.mjpeg
```


For the older versions (before R0-4) see the table below:
|   URL  | Available from |  Info  |
| ------ | -------------- | ------ |
| https://yourserver.tld/CAMERA_NAME/raw.mjpeg | R0-1 | re-streaming of the original mjpeg stream |
| https://yourserver.tld/CAMERA_NAME/stream.mjpeg | R0-2 | stream of the raw jpegs, reusing last frame of timeouts |
| https://yourserver.tld/CAMERA_NAME/last.jpeg | R0-1 | last known jpeg file |
| https://yourserver.tld/CAMERA_NAME/next.jpeg | R0-1 | next jpeg file |
| https://yourserver.tld/CAMERA_NAME/valid.jpeg | R0-2 | last known valid jpeg file |
| https://yourserver.tld/CAMERA_NAME/client.json | R0-2 | client (remote camera) status and stats |
| https://yourserver.tld/CAMERA_NAME/rpi06.jpeg | R0-3 | image adapted for rpi06 streaming |
| https://yourserver.tld/CAMERA_NAME/rpi06.mjpeg | R0-3 | mjpeg adapted for rpi06 streaming |


## Release info

Releases `R0-yy-zz` are first development releases. The plan is to generate a new release once a new feature from the list at the top of this file is implemented and there are no known (critical) issues exist. Once the list is implemented (or functionality is on par with the old Node.js  code) the release will be changed to `R0-yy-zz`. 

Generally the following versioning scheme will be used: `Rxx-yy-zz`. `R` stands for release tag, `xx` is main version number. This number subject to change on in case of major refactoring or modification of the core part of this project. New releases will not guaranty API compatibility and most likely will introduce some breaking changes. `yy` is a minor version. This number increases as a new features will be added. No breaking changes are expected, but over time some can be become obsolete/depreciated. Depreciated API will be still supported (without newer features) for at least 1 year. `zz` is patch level. When no functionality was add but some bugs where fixed this is a number which would be increased. `zz` is only added if it is not zero.

During `R0` phase breaking changes can happen between minor releases in order to accommodate all needed.

| Release | Info |
| ------- | ---- |
| R0-1    | Basic functionality: raw mjpeg stream, last and next jpg images |
| R0-2    | JPEG validation, remote client status, requirements.txt |
| R0-3    | Several bugfixes found with a first deployment, rpi06 special adaptations |
| R0-4    | more bugfixes, using jpegtran for to downscale images, rpi06 will use next / new frame |
| R0-5    | camera statistics and status |
| R0-6 (WiP)   | latency statistics, converted to a python module |




