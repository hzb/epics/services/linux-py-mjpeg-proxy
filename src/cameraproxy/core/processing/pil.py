from .base import Pipeline
from PIL import Image, ImageOps
import io

class PILPipeline(Pipeline):
    def __init__(self,frame) -> None:
        self._image = Image.open(io.BytesIO(frame))
        pass

    def dispose(self):
        self._image.close()
        del self._image
        return super().dispose()

    def bytes(self):
        img_byte_arr = io.BytesIO()
        self._image.save(img_byte_arr, format='JPEG')
        return img_byte_arr.getvalue()

    def equalize(self):
        self._image = ImageOps.equalize(self._image)
        return self

    def grayscale(self):
        self._image = ImageOps.grayscale(self._image)
        return self

    def fit(self,width,height):
        self._image = ImageOps.fit(self._image,(width,height))
        return self

    def autocontrast(self,cutoff=2):
        self._image = ImageOps.autocontrast(self._image,cutoff=cutoff)
        return self

