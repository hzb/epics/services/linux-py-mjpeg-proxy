from  datetime import datetime

class Frame(bytearray):
    """
    Wrapper for the jpeg data frame with additional information tracking
    """
    def __init__(self,*args,original=None,**kwargs) -> None:
        super().__init__(*args,**kwargs)
        if original is None:
            self._timestamp = datetime.now()
        else:
            self._timestamp = original._timestamp
        
    def elapsed(self) -> float:
        """
        get time passed since creation in milliseconds
        """
        _now = datetime.now()
        return (_now-self._timestamp).total_seconds()

    def created(self) -> float:
        """
        creation timestamp
        """
        return self._timestamp


    def copy(self) -> bytearray:
        return Frame(super().copy(),original=self)