class Something:
    def __init__(self,_dict,_id) -> None:
        self._dict = _dict
        self._id = _id
        self._dict[_id] = self

    def dispose(self):
        self._dict.pop(self._id)

d = {}
s = Something(d,"k1")
