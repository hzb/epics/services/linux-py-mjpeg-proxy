from statistics import mean
from PIL import Image,ImageOps
import timeit
import cProfile
import io

from jpegtran import JPEGImage

def pil_test(filename):
    with Image.open(filename) as image:
        width = 1280
        height = 1024
        processed = ImageOps.fit(image,(width,height))
        img_byte_arr = io.BytesIO()
        processed.save(img_byte_arr, format='JPEG')
        return img_byte_arr.getvalue()

def jpegtran_test(filename):
    img = JPEGImage(filename)
    width = 1280
    height = 1024
    return img.downscale(width,height).as_blob()

def time_runs(func, filename, preambula=None):
    t = timeit.Timer(lambda: func(filename))
    repeat, number = 10, 10
    r = t.repeat(repeat, number) 
    best, worst, avg = min(r), max(r), mean(r)
    best_mspc = best/number*1000
    worst_mspc = worst/number*1000
    avg_mspc = avg/number*1000
    
    if preambula is None:
        preambula = f"{func}('{filename}'):: "
    else:
        preambula = preambula+":: "
    print(f"{preambula}Stats from {number} loops, {repeat} times:")
    print(f"{preambula}mean  time: {avg_mspc:.3g} ms per call")
    print(f"{preambula}best  time: {best_mspc:.3g} ms per call")
    print(f"{preambula}worst time: {worst_mspc:.3g} ms per call")

    #print(f"{preambula}Status: {last_validation_status()}")
    #print(f"{preambula}Quick : {quick_check(img_data)}")
    #print(f"{preambula}QuickStr: {quick_check_debug(img_data)}")


    print()

filename = "sample.jpeg"

time_runs(pil_test,filename)
time_runs(jpegtran_test,filename)