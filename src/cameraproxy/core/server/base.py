from threading import current_thread
from flask import Response,Flask, stream_with_context, request, jsonify,json

from ..client import MJPEGClient
from ..mjpeg import Muxer
from ..utils import make_logger
from ..messagequeue import MessageQueue
from ..cache import JPEGCache
from ..exceptions import TimeoutError

from ..frame import Frame

from datetime import datetime

def call_once(generator):
        for value in generator:
                return value

class MJPEGProxyBase:
    """
    base class for the MJPEG  proxy.
    it was separated from MJPEGProxy mostly to keep separation between basic/core functions 
    and particular implementations for better code readability
    """
    def __init__(self,flaskapp:Flask,name,config) -> None:
        self._log = make_logger(__name__+"("+name+")")
        self.name = name
        self.app = flaskapp
        if "description" in config:
            self.description = config["description"]
        else:
            self.description = name
        self._urls = []
        self._url_groups = []

        # some config options - to be implemented in a config file
        self._mjpeg_frame_timeout = 3.0

        # prepare message queue
        self._mq = MessageQueue()
        #self._mq.start()

        # create MJPEG client (consume external MJPEG stream) and start it in a thread
        self._client = MJPEGClient(name,config,self._mq)
        self._client.start()
        self.register_json_generator("client",       self._client_state_json, title="Client state",hide=True)

        # start caching / monitoring for the last known image
        self._cache = JPEGCache(self._mq)
        self._cache.start()


    def _register_url_callback2(self,endpoint,method,title=None, hide=False,url_group=None,short_name="???"):
        """
        this method used to link a processing url to a callback function
        """
        url = "/"+self.name+"/"+endpoint
        if title is not None:
            self._urls.append((url,title,hide))
            if url_group is not None:
                url_group.append((url,short_name,hide))
        self.app.route(url,endpoint=self.name+"/"+endpoint)(method)

    def register_image_generator(self,name,generator,title=None, hide=False):
        """
        This function is used to register image generator function.
        It will register with flask several URLs for jpeg and mjpeg streams

        The callback is expected to be a generator function which produce a stream of the frames.
        For the jpeg part it will be called only once and then interrupted. MJPEG stream will run until user stops accepting data

        """
        if title is None:
            title = name
        url_group = []
        self._register_url_callback2(name+".jpeg",lambda: self.send_jpeg_response(generator()),title=f"{title} (JPEG)",hide=hide,url_group=url_group,short_name='JPEG')
        self._register_url_callback2(name+".jpg",lambda: self.send_jpeg_response(generator()),title=f"{title} (JPG)",hide=True,url_group=url_group,short_name='JPG')
        self._register_url_callback2(name+".mjpeg",lambda: self.send_mjpeg_response(generator()),title=f"{title} (MJPEG)",hide=hide,url_group=url_group,short_name='MJPEG')
        self._register_url_callback2(name+".mjpg",lambda: self.send_mjpeg_response(generator()),title=f"{title} (MJPG)",hide=True,url_group=url_group,short_name='MJPG')
        self._register_url_callback2(name+".latency.json",lambda: self.send_latency_response(generator()),title=f"{title} (LATENCY)",hide=True,url_group=url_group,short_name='LATENCY')
        self._register_url_callback2(name+".latency.stream.json",lambda: self.send_latency_stream_response(generator()),title=f"{title} (LATENCY stream)",hide=True,url_group=url_group,short_name='LATENCY (stream)')
        self._url_groups.append((title,url_group,hide))

    def register_json_generator(self,name,generator,title=None, hide=False):
        """
        this method used to link a processing url to a callback function
        """
        if title is None:
            title = name
        url_group = []
        self._register_url_callback2(name+".json",lambda: call_once(generator()),title=f"{title} (JSON)",hide=hide,url_group=url_group,short_name='JSON')
        self._url_groups.append((title,url_group,hide))


    def _client_state_json(self):
        """
        generator for the client state information
        """
        yield jsonify(self._client.stats())

    @property
    def urls(self):
        """
        return dictionary of known/register URLs and its descriptions
        This method thought to be for the template processing so possible
        links are defined in this class and not in the template
        """
        return self._urls

    @property
    def url_groups(self):
        """
        """
        return self._url_groups

    def send_latency_response(self,frame_generator):
        """
        handler for latency tracking info
        """
        try:
            frame:Frame = call_once(frame_generator)
            info = {
                'latency':frame.elapsed(),
                'created':frame.created()
            }
            return jsonify(info)
        except TimeoutError:
            self._log.error(f"Timeout reading next frame")
        except Exception as e:
            self._log.error(f"Error while processing jpeg response, error was: {type(e)}::{str(e)}")
        return Response(status=503)

    def send_latency_stream_response(self,frame_generator):
        """
        handler for motion jpeg streams
        """
        _stop_request = False

        @stream_with_context
        def stream():
            try:
                frame:Frame
                last_ts = datetime.now()
                for frame in frame_generator:
                    new_ts = datetime.now()
                    if _stop_request: 
                        self._log.info("CLOSING (Breaking on stop request)")
                        break
                    info = {
                        'generation_latency':(new_ts-last_ts).total_seconds(),
                        'pipeline_latency':frame.elapsed(),
                        'created':frame.created()
                    }
                    last_ts=new_ts
                    yield json.dumps(info)+"\r\n"
            except Exception as e:
                self._log.info(f"CLOSING (on error), error was: {type(e)}::{str(e)}")

        def stop():
            self._log.info("CLOSING (on normally)")
            _stop_request = True

        r =  Response(stream())
        r.mimetype = "application/json"
        r.call_on_close(stop)
        return r

    def send_jpeg_response(self,frame_generator):
        """
        handler for jpeg files
        """

        try:
            return Response(content_type="image/jpeg",response=call_once(frame_generator))
        except TimeoutError:
            self._log.error(f"Timeout reading next frame")
        except Exception as e:
            self._log.error(f"Error while processing jpeg response, error was: {type(e)}::{str(e)}")
        return Response(status=503)

    def send_mjpeg_response(self,frame_generator):
        """
        handler for motion jpeg streams
        """
        _stop_request = False
        muxer = Muxer()
        remote_ip = request.environ['REMOTE_ADDR']
        remote_port = request.environ['REMOTE_PORT']
        self._log.info(f"Starting client stream for {remote_ip}:{remote_port} in {current_thread().getName()}")

        @stream_with_context
        def stream():
            index = 0
            try:
                for frame in frame_generator:
                    if _stop_request: break
                    index += 1
                    self._log.debug(f"Sending frame #{index} len={len(frame)} on channel '{self.name}' to {remote_ip}:{remote_port}")
                    yield muxer.encode(frame)
            except Exception as e:
                self._log.info(f"CLOSING (on error), error was: {type(e)}::{str(e)}")

        def stop():
            self._log.info("CLOSING (on normally)")
            _stop = True

        r =  Response(stream())
        r.mimetype = muxer.mimetype
        r.headers.update(muxer.response_headers)
        r.call_on_close(stop)
        return r
