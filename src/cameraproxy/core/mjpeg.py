"""
MJPEG muxer/demuxer and etc

(c) 

"""
import re
from .utils import make_logger
from .exceptions import MJPEGError

BOUNDARY_PATTERN="multipart/x-mixed-replace; *boundary=(.*)"
INTERNAL_BUFFER_SIZE=1024*1024*8

class Demuxer:
    """
    This class takes http(s) stream and demuxes it MJPEG stream
    """
    def __init__(self,stream) -> None:

        # configure logger
        self._log = make_logger(__name__)

        self.stream =  stream
        headers = stream.info()
        content_type = headers['Content-Type']
        self._log.debug(f'content_type is: {content_type}')
        if match:=re.search(BOUNDARY_PATTERN,content_type,re.IGNORECASE):
            boundary = "--"+match.group(1).removeprefix("--")
            self._log.debug(f'Boundary is: {boundary}')
        else:
            raise MJPEGError("Bad content type or no boundary defined")
        self.boundary = boundary

    def read_frame(self):
        """
        read single frame from the current stream and return it as a bytearray
        """
        
        # read boundary/read all headers/read empty line
        headers = self._read_frame_headers()
        # get length
        content_length_str = headers.get('Content-Length')
        #content_length = None
        try:
            content_length = int(content_length_str)
        except ValueError:
            raise MJPEGError("Error reading 'Content-Length' for the next frame")
        # read LENGTH bytes
        payload = self._read_n_bytes(content_length)
        # read empty line
        self._read_single_line()
        return payload

    def _read_n_bytes(self,n):
        ba = bytearray(n)
        n_read = self.stream.readinto(ba)
        if n_read!=n:
            raise MJPEGError(f"Error while reading frame, expected {n} bytes, read {n_read} bytes")
        return ba

    def _read_frame_headers(self):
        """
        Read headers for the current section of the stream and return them as an array

        Headers normally would look like:

        '''
        --boundarydonotcross
        Content-Type: image/jpeg
        Content-Length: 518816
        X-Timestamp: 399406.394608

        '''
        they start with boundary marker, then comes one or more headers. Required
        are 'Content-Length' and 'Content-Type' but there can be more.
        Finally an empty line comes to mark end of the header section

        """
        #line0 = self.read_single_line()
        #print(f"DEBUG:: line0 is {line0}")
        boundary = self._read_single_line()
        if boundary=='':
            #self._log.debug(f"extra empty line detected, skipping it ")
            boundary = self._read_single_line()
        #self._log.debug(f"boundary is '{boundary}', self.boundary is '{self.boundary}'")
        #print(f"boundary is '{boundary}', self.boundary is '{self.boundary}'")
        if boundary != self.boundary:
            raise MJPEGError("Missing boundary marker!")

        headers = {}
        while (line := self._read_single_line())!='':
            k,v = line.split(':',1)
            headers[k] = v

        return headers

    def _read_single_line(self):
        """
        read single line of a (header) from the stream
        """
        return self.stream.readline().decode('utf-8').strip()

class Muxer:
    """
    Simple class to mux data info mjpeg stream
    """
    def __init__(self, boundary = 'mjpegframe') -> None:
        self._frame_headers = []
        self._boundary = boundary#"mjpegframe"
        
        self._header_preamble = f'--{self._boundary}\r\nContent-Type: image/jpeg\r\nContent-Length: '
    
    @property
    def mimetype(self):
        """
        mime type string 
        """
        return f'multipart/x-mixed-replace; boundary=--{self._boundary}'

    @property
    def response_headers(self):
        """
        Dictionary with suggested response headers
        """
        headers = {}
        headers['Cache-Control'] = 'private, no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0'
        headers['Pragma'] = 'no-cache'
        return headers

    def encode(self, payload):      
        hdr = bytes(f'{self._header_preamble}{len(payload)}\r\n\r\n',encoding='utf-8')
        return (hdr + bytes(payload) + b'\r\n')
