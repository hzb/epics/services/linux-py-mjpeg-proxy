"""
JPEG encoding/decoding / validation

the code is inspired and based on the great article: https://yasoob.me/posts/understanding-and-writing-jpeg-decoder-in-python/
and repo from the there: https://github.com/yasoob/Baseline-JPEG-Decoder
"""
from struct import unpack
import struct
import re

marker_mapping = {
    0xffd8: "Start of Image",
    0xffe0: "Application Default Header",
    0xffdb: "Quantization Table",
    0xffc0: "Start of Frame",
    0xffc4: "Define Huffman Table",
    0xffda: "Start of Scan",
    0xffd9: "End of Image"
}

parameterless_markers = (
    0xFFD8, # SOI
    0xFFD9, # EOI
    0xFFD0,
    0xFFD1,
    0xFFD2,
    0xFFD3,
    0xFFD4,
    0xFFD5,
    0xFFD6,
    0xFFD7,
)

def marker_str(marker):
    return marker_mapping.get(marker,str(marker))

unpack_BB = struct.Struct('BB').unpack
unpack_mH = struct.Struct('>H').unpack

class ChunkIterator:
    def __init__(self, data):
        self._raw_data = data

    def __iter__(self):
        self._next_data = self._raw_data
        return self

    def __next__(self):
        data = self._next_data
        if data is None:
            raise StopIteration
        (marker,) = unpack_mH(data[0:2])
        if marker in parameterless_markers:
            #print(f"DEBUG:: Parameterless marker: {marker}::{marker_str(marker)}")
            if marker==0xFFD9:
                self._next_data = None
            else:
                self._next_data = data[2:]                
            return marker,None,None
        (length,) = unpack_mH(data[2:4])
        length+=2
        if marker == 0xFFDA:
            payload,length_ext =  self._parse_SAS_data(data[length:])
            length+=length_ext
            pass
        else:
            payload = data[4:length]
        #print(f"DEBUG:: Marker: {marker}::{marker_str(marker)}, length={length}")
        self._next_data = data[length:]
        return marker,length,payload

    def __nextold__(self):
        data = self._next_data
        if data is None:
            raise StopIteration
        (marker,) = unpack_mH(data[0:2])
        if marker in parameterless_markers:
            #print(f"DEBUG:: Parameterless marker: {marker}::{marker_str(marker)}")
            if marker==0xFFD9:
                self._next_data = None
            else:
                self._next_data = data[2:]                
            return marker,None,None
        (length,) = unpack_mH(data[2:4])
        length+=2
        if marker == 0xFFDA:
            payload,length_ext =  self._parse_SAS_data(data[length:])
            length+=length_ext
            pass
        else:
            payload = data[4:length]
        #print(f"DEBUG:: Marker: {marker}::{marker_str(marker)}, length={length}")
        self._next_data = data[length:]
        return marker,length,payload

    def _parse_SAS_data(self,data):
        """
        Removes 0x00 after 0xff in the image scan section of JPEG
        """
        matchObj = re.match( b'(.*)\xff[^\x00].*', data, re.MULTILINE|re.DOTALL)
        return matchObj.group(1),len(matchObj.group(1))


def validate_full(data):
    def _get_SOS_len(_data):
        matchObj = re.match( b'(.*)\xff[^\x00].*', _data, re.MULTILINE|re.DOTALL)
        return len(matchObj.group(1))

    if data is None:
        return False
    (marker,) = unpack_mH(data[0:2])
    if marker in parameterless_markers:
        if marker==0xFFD9:
            return True
        else:
            return validate_full(data[2:])
    (length,) = unpack_mH(data[2:4])
    length+=2
    if marker == 0xFFDA:
        length_ext =  _get_SOS_len(data[length:])
        length+=length_ext
    return validate_full(data[length:])

class ValidationIterator:
    def __init__(self,data) -> None:
        self._raw_data = data
        self.EOI = False

    def __iter__(self):
        self._data = self._raw_data
        self.EOI = False
        return self

    def __next__(self):
        def _get_scan_length(_data):
            matchObj = re.match( b'(.*)\xff[^\x00].*', _data, re.MULTILINE|re.DOTALL)
            return len(matchObj.group(1))

        data = self._data

        if data is None:
            raise StopIteration

        (marker,) = unpack_mH(data[0:2])
        if marker in parameterless_markers:
            if marker==0xFFD9:
                self._data = None
                self.EOI = True
                return True
            else:
                self._data = data[2:]
                return False
        (length,) = unpack_mH(data[2:4])
        length+=2
        if marker == 0xFFDA:
            scan_legnth =  _get_scan_length(data[length:])
            length+=scan_legnth
        self._data = data[length:]
        return False

def validate(img_data):
    try:
        vi = ValidationIterator(img_data)
        for st in vi:pass
    except:
        return False
    return vi.EOI

