## install dependencies:

```sh
pip install flask   # web server
pip install asgiref # async io for flask
pip install pyopenssl # https for flask
# pip install event-channel # testing replacement for Queue

pip3 install --upgrade Pillow
```

## Run flask:

```bash
flask --app=hello run  --host=0.0.0.0 
```

## old template
body
      center
        h1 
        table(cellspacing="15px")
          tbody
              for camera,name in cameras
                  tr
                      td
                          h2=camera.description
                          img(src=name+".jpg" height="192px")
                          p
                            a(href=name+".jpg") Latest JPG image
                          p
                            a(href="valid/"+name+".jpg") Latest <b><u>VALID</u></b> JPG image
                          p
                            a(href=name+".mjpg") Live MJPG stream


## Faster JPEG images:

https://github.com/jbaiter/jpegtran-cffi
https://pypi.org/project/simplejpeg/


## There was an error happening over the night. Stream still works now, although

ERROR:: Unhandled exception in client http://ldp-rpi16.exp.helmholtz-berlin.de:9090/?action=stream : int() argument must be a string, a bytes-like object or a real number, not 'NoneType'
Traceback (most recent call last):
  File "/home/ovsyannikov/projects/py-camera-proxy/cameraproxy/client.py", line 55, in _acquire_loop
    self._demux_mjpeg_stream(stream)
  File "/home/ovsyannikov/projects/py-camera-proxy/cameraproxy/client.py", line 39, in _demux_mjpeg_stream
    self._queue.put(demuxer.read_frame())
  File "/home/ovsyannikov/projects/py-camera-proxy/cameraproxy/mjpeg.py", line 45, in read_frame
    content_length = int(content_length_str)
TypeError: int() argument must be a string, a bytes-like object or a real number, not 'NoneType'
ERROR:: Unhandled exception in client http://ldp-rpi16.exp.helmholtz-berlin.de:9091/?action=stream : int() argument must be a string, a bytes-like object or a real number, not 'NoneType'
Traceback (most recent call last):
  File "/home/ovsyannikov/projects/py-camera-proxy/cameraproxy/client.py", line 55, in _acquire_loop
    self._demux_mjpeg_stream(stream)
  File "/home/ovsyannikov/projects/py-camera-proxy/cameraproxy/client.py", line 39, in _demux_mjpeg_stream
    self._queue.put(demuxer.read_frame())
  File "/home/ovsyannikov/projects/py-camera-proxy/cameraproxy/mjpeg.py", line 45, in read_frame
    content_length = int(content_length_str)
TypeError: int() argument must be a string, a bytes-like object or a real number, not 'NoneType'


## JPEG info

https://github.com/corkami/formats/blob/master/image/jpeg.md
https://www.disktuna.com/list-of-jpeg-markers/

https://svn.xiph.org/experimental/giles/jpegdump.c
https://github.com/libjpeg-turbo/libjpeg-turbo

https://gitlab.com/jfolz/simplejpeg/-/blob/master/simplejpeg/__init__.py


## turbo jpeg
sudo apt install libturbojpeg  libturbojpeg0-dev

pip install jpegtran-cffi


## pip comment

list packages which are not required / not installed as dependency:

pip list  --not-required
