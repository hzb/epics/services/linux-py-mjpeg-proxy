"""
JPEG encoding/decoding / validation
"""
import struct
import re

PARAMETERLESS = (
    0xFFD8, # SOI
    0xFFD9, # EOI
    0xFFD0,
    0xFFD1,
    0xFFD2,
    0xFFD3,
    0xFFD4,
    0xFFD5,
    0xFFD6,
    0xFFD7,
)

unpack_mH = struct.Struct('>H').unpack
class ValidationIterator:
    def __init__(self,data) -> None:
        self._raw_data = data
        self.EOI = False
        self.SOI = False
        self.scan_length_re = re.compile(b'(.*)\xff[^\x00].*', re.MULTILINE|re.DOTALL)

    def __iter__(self):
        self._data = self._raw_data
        self.EOI = False
        self.SOI = False
        return self

    def __next__(self):
        def _get_scan_length(_data):
            matchObj = self.scan_length_re.match(_data)
            if matchObj is None:
                return len(_data)
            return len(matchObj.group(1))

        data = self._data

        if data is None:
            raise StopIteration

        (marker,) = unpack_mH(data[0:2])
        if marker in PARAMETERLESS:
            if marker==0xFFD9:
                self._data = None
                self.EOI = True
                return True
            elif marker==0xFFD8:
                self._data = data[2:]
                self.SOI = True
                return False
            else:
                self._data = data[2:]
                return False
        (length,) = unpack_mH(data[2:4])
        length+=2
        if marker == 0xFFDA: # SOS / start-of-scan
            scan_legnth =  _get_scan_length(data[length:])
            length+=scan_legnth
        self._data = data[length:]
        return False

LastValidationStatus = None
__jpeg_quick_re = re.compile(b'^\xFF\xD8.*\xff\xD9[\x00]*$', re.MULTILINE|re.DOTALL)
def quick_check(data):
    """
    quick check if data is a valid JPEG by checking first and the last bytes of the data
    this function is less strict and allows extra zero at the end
    """
    return __jpeg_quick_re.match(data) is not None

def quick_check_strict(data):
    """

    this is a "strict" validation of the JPEG files
    unfortunately it will fail in some cases (for example MJPEG streamer adds 4 zero bytes at the end)
    """
    return data[:2] == b'\xFF\xD8' and data[-2:] == b'\xFF\xD9'

def quick_check_debug(data):
    start = data[:2]
    start_marker =  b'\xFF\xD8'
    end = data[-2:]
    end_marker=b'\xFF\xD9'
    return f"start={start}, start_marker={start_marker}, end={end}, end_marker={end_marker}"

def validate(img_data):
    """
    Check ig JPEG is a valid

    param:
    img_data - bytes like object with jpeg data
    """
    global LastValidationStatus
    if quick_check(img_data):
        try:
            vi = ValidationIterator(img_data)
            for st in vi:pass
            LastValidationStatus = vi.EOI and vi.SOI
            return vi.EOI and vi.SOI
        except Exception as e:
            LastValidationStatus = e#"Exception"
            return False
    else:
        LastValidationStatus = "Not a JPEG"
        return False

def last_validation_status():
    """
    last known validation status, used for debugging
    """
    return LastValidationStatus