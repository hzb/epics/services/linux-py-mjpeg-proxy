from configparser import ConfigParser
import json

from collections.abc import MutableMapping

CameraTypeMapping = {
    "enabled": bool
}

class Camera(MutableMapping):
    """
    camera interface implementation
    """
    def __init__(self, *args, **kwargs):
        self.__dict = dict()
        self.update(dict(*args, **kwargs))  # use the free update to set keys

    def __getitem__(self, key):
        return self.__dict[key]

    def __setitem__(self, key, value):
        if key in CameraTypeMapping:
            value = CameraTypeMapping[key](value)
        self.__dict[key] = value

    def __delitem__(self, key):
        del self.__dict[key]

    def __iter__(self):
        return iter(self.__dict)
    
    def __len__(self):
        return len(self.__dict)

    def __str__(self):
        return str(self.__dict)

# class Camera(dict):
#     pass

class ConfigBase:
    """
    this is a base class for the config implementations, particular formats
    should inherit from this class and override special methods

    `def _string_to_config(self)` to parse config contect as string and return dict of dicts

    `def _config_to_string(self)` to save config data


    """
    def __init__(self,filename=None,/,autoload=True,*args,**kwargs) -> None:
        self._filename = filename
        self._args = args
        self._kwargs = kwargs
        self._cameras = {}
        if autoload:
            self.load()

    def cameras(self):
        return self._cameras

    def load(self):
        """
        load and parse config file
        """
        if self._filename is not None:
            _dict = self._file_to_config(self._filename)
        if _dict is not None:
            for k,v in _dict.items():
                self._cameras[k] = Camera(v)

    # Overridable API
    def _file_to_config(self,filename):
        with open(filename,"r") as f:
            file_content = f.read()
        return self._string_to_config(file_content)

    def _string_to_config(self,config_str):
        """
        convert given string into config 
        """
        pass


    def _config_to_file(self,filename):
        raise NotImplementedError("Config saving is not yet supported")

    def _config_to_string(self):
        raise NotImplementedError("Config saving is not yet supported")
        #return ""


class INIConfig(ConfigBase):
    pass

class JSONConfig(ConfigBase):
    """
    support for the JSON format of the configuration files
    """
    def _string_to_config(self, config_str):
        result = json.loads(config_str)
        return result
