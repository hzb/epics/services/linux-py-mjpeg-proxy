from threading import Thread
import time

from .mjpeg import MJPEGError, Demuxer
from .camera import HTTPCamera
from .messagequeue import MessageQueue
from .utils import make_logger
from .exceptions import MJPEGError,NetworkError

from .statistics import FrameStats
from .frame import Frame

import traceback

class MJPEGClient:
    def __init__(self,name,config,mq:MessageQueue) -> None:
        self._name = name
        self._log = make_logger(f"{__name__}({name})")
        self._camera = HTTPCamera(config)
        self._queue = mq.publisher() # get publisher queue
        self._should_run = True
        self._acquire_thread = Thread(target=self._acquire_loop)
        self._reconnect_time = 1
        self._connected = False
        self._fail_attempts = 0   # for auto-tuning reconnect times
        self._reconnects = 0        # for general stats
        self._stats = FrameStats(average=20)


    def start(self):
        if self._acquire_thread.is_alive():
            self._log.warning(f"Thread already started!")
        else:
            self._acquire_thread.start()

    def  _demux_mjpeg_stream(self,stream):
        """
        handle incoming MJPEG stream
        """
        demuxer = Demuxer(stream)
        frame_counter = 0
        while self._should_run:
            frame_counter+=1
            #self._log.debug(f"Frame counter {frame_counter}", end='\r')
            if self._queue.full():
                self._log.warning("Queue full, dropping oldest frame")
                self._queue.get()
            self._queue.put(Frame(demuxer.read_frame()))
            self._stats.next()

    def _acquire_loop(self):
        """
        This method runs in a separate thread by multithreading.Thread
        """
        self._stop = False

        while not self._stop:
            try:
                self._stats.reset()
                with self._camera.mjpeg_stream() as stream:
                    if stream.status != 200:
                        raise MJPEGError(f'Invalid response from server at url "{self._name}": {stream.status}')
                    self._reconnects += 1
                    self._fail_attempts = 0
                    self._connected = True
                    self._demux_mjpeg_stream(stream)
            except EOFError: # happens when stream is closed
                self._fail_attempts += 1
            except NetworkError: # problem opening connection
                self._fail_attempts += 1
            except Exception as e:
                self._log.warning(f'Unhandled exception "{type(e)}" in client "{self._name}":  {e}')
                self._fail_attempts += 1
                #traceback.print_exc()
            finally:
                self._connected = False

            time.sleep(self._reconnect_time)


    def stats(self):
        """
        return some basic connection statistics
        """
        return {
            'connected' : self._connected,
            'reconnects' : self._reconnects,
            'fails' : self._fail_attempts,
            'url' : self._camera.url,
            'fps' : self._stats.fps()
        }
        