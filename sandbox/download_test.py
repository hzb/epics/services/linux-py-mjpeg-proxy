
from  urllib.request import urlopen
import time

def get_one(url):
    r = urlopen(url).read()
    return r

def time_download(url,n=50):
    start = time.time()
    for i in range(0,n):
        get_one(url)
    elapsed = time.time()-start
    fps = n/elapsed
    print(f'Reading {n} frames from "{url}" took {elapsed:.2f} seconds, fps={fps:.2f}')


time_download("http://rovm-asrv02.exp.helmholtz-berlin.de:9090/pm4mono/next.jpg")
time_download("http://rovm-asrv02.exp.helmholtz-berlin.de:9090/pm4mono/last.jpg")

time_download("http://rovm-asrv02.exp.helmholtz-berlin.de:9090/chopperslit/next.jpg")
time_download("http://rovm-asrv02.exp.helmholtz-berlin.de:9090/chopperslit/last.jpg")

time_download("http://rovm-asrv02.exp.helmholtz-berlin.de:9090/ldp-side/next.jpg",n=20)
time_download("http://rovm-asrv02.exp.helmholtz-berlin.de:9090/ldp-side/last.jpg",n=20)