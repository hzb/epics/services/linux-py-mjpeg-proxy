from queue import Queue
import threading
import time

class MessageQueue:pass

class DisposableQueue(Queue):
    """
    DisposableQueue allows to use "with queue.subscriber():" syntax and ensures
    that queues which are not in use any more can be safely deregistered
    """
    def __init__(self, _dict:dict,_id:str, mutex, maxsize: int = 0) -> None:
        super().__init__(maxsize)
        self._mutex = mutex
        self._dict = _dict
        self._id = _id

    def dispose(self):
        with self._mutex:
            self._dict.pop(self._id)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.dispose()

class MessageQueue:
    """
    This is a publisher/subscriber management class

    #publisher 
    """
    def __init__(self, maxpubmsgs:int=25, maxsubmsgs:int=1, loop_sleep=0.001, autostart=True) -> None:
        self.__publisher_queues = {}
        self.__subscriber_queues = {}
        self.__maxpubmsgs = maxpubmsgs
        self.__maxsubmsgs = maxsubmsgs
        self.__iothread = threading.Thread(target=self.__loop)
        self.__loop_sleep = loop_sleep
        self.__pubmutex  = threading.Lock()
        self.__submutex  = threading.Lock()
        if autostart:
            self.start()

    def publisher(self, id:str = None) -> DisposableQueue:
        """
        Get publisher queue for selected id. If id is unknown a new publisher will be registered
        """
        return self.__register(id,self.__publisher_queues,self.__pubmutex, self.__maxpubmsgs)

    def subscriber(self, id:str = None) -> DisposableQueue:
        """
        Get subscriber queue for selected id. If id is unknown a new subscriber will be registered
        """
        return self.__register(id,self.__subscriber_queues,self.__submutex, self.__maxsubmsgs)

    # IO thread
    def start(self):
        """
        start IO thread, if it is not yet started
        """
        ...
        if self.__iothread.is_alive():
            return True
        self.__iothread.start()

    # Looping methods
    def __loop(self):
        while True:
            with self.__pubmutex:
                for payload in self.__pull():
                    self.__push(payload)

    def __push(self,payload):
        """
        Put data into subscriber queue
        If the queue is full then do nothing (drop payload)
        """
        with self.__submutex:
            s:Queue # just type hinting for development
            for s in self.__subscriber_queues.values():
                if not s.full():
                    s.put(payload)

    def __pull(self):
        """
        cycle over all publishers and pull all data
        """
        p:Queue # just type hinting for development
        yielded = False
        for p in self.__publisher_queues.values():
            if p.not_empty:
                yielded = True
                yield p.get()
        if not yielded:
            time.sleep(self.__loop_sleep)

    # Internal utility methods. 
    def __register(self, id, queue_dict, mutex, maxsize) -> DisposableQueue:
        """
        publisher/subscriber registration helper 

        creates a new queue or returns already created Queue for a given ID
        If no ID supplied it defaults to current thread name
        """
        if id is None:
            id = threading.current_thread().getName()
        with mutex:
            if id not in queue_dict.keys():
                queue_dict[id] = DisposableQueue(queue_dict,id,mutex,maxsize)
            return queue_dict[id]


