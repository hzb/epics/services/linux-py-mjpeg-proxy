from cameraproxy.client import MJPEGClient

#c = MJPEGClient("https://www.google.com")
#c = MJPEGClient("http://ldp-rpi05.exp.helmholtz-berlin.de:9090/?action=stream")
#c = MJPEGClient("http://ldp-hwctl02.exp.helmholtz-berlin.de:20004/SAMPLE.MJPG.mjpg")
proxy_sample = "http://ldp-hwctl02.exp.helmholtz-berlin.de:20004/SAMPLE.MJPG.mjpg"
mono = {
    "url" : "http://pm4mono.exp.bessy.de/ipcam/mjpeg.cgi",
    "username" : "user",
    "password" : "field" 
    }

#c = MJPEGClient(mono['url'], user=mono['username'],password=mono['password'])
c = MJPEGClient(mono)
#c = MJPEGClient(proxy_sample)

c.run_once()

"""
mjpeg-streamer:

[('Access-Control-Allow-Origin', '*'), ('Connection', 'close'), ('Server', 'MJPG-Streamer/0.2'), ('Cache-Control', 'no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0'), ('Pragma', 'no-cache'), ('Expires', 'Mon, 3 Jan 2000 12:34:56 GMT'), ('Content-Type', 'multipart/x-mixed-replace;boundary=boundarydonotcross')]


"""