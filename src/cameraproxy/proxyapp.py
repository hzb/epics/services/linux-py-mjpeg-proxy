from flask import Flask
from flask import request, render_template
from markupsafe import escape
from .core.server import MJPEGProxy
from .core.config import JSONConfig

from .core.path_parser import ParsePath

import logging

webapp = Flask(__name__)
servers = {}

@webapp.route("/path/", defaults={'relpath': 'vers2'})
@webapp.route("/path/<path:relpath>", methods=['GET','POST'])
def test_url(relpath):
    print(f"Path(str): {relpath}")
    print(f"ParsePath: {ParsePath(relpath)}")
    return f"<p>Hello, Test!</p><p>Path:<br/>{relpath}</p><p>Args:<br/>{escape(request.args)}</p><p>environ:<br/>{request.environ}</p>"

@webapp.route("/fail")
def fail_test():
    raise Exception("Failed as expected!")

@webapp.route('/test')
@webapp.route('/test.html')
def test_page():
    debug =  request.args.get('debug',0,type=int)
    #print(f"DEBUG::debug={debug}")

    if debug>5: # local print info for the debug levels above 5
        for server in servers.values():
            for title,group,hide in server.url_groups:
                print(f"{title}::",end="")
                for url,label in group:
                    print(f"[{label}]",end="")
                print()
    return render_template('test.jinja', title='List of known cameras', servers = servers, debug=debug)

@webapp.route('/')
def main_page():
    debug =  request.args.get('debug',0,type=int)
    return render_template('index.jinja', title='List of known cameras', servers = servers, debug=debug)


def main():
    config = JSONConfig("cameras.json")
    global servers
    for name,camera in config.cameras().items():
        if camera['enabled']:
            print(f"Starting camera: {name}")
            servers[name] = MJPEGProxy(webapp,name,camera)
        else:
            print(f"Camera {name} is disabled, skipping it")

    # for 'production-testing' prevent from writing too many logs
    werkzeug_logger = logging.getLogger('werkzeug')
    werkzeug_logger.setLevel(logging.WARNING)

    webapp.run(host='0.0.0.0', port=9090, threaded=True)

if __name__ == "__main__":
    main()

