from statistics import mean
from cameraproxy.jpeg import validate,last_validation_status,quick_check,quick_check_debug
import sys
import timeit
import cProfile

def time_runs(img_data, preambula=None):
    t = timeit.Timer(lambda: validate(img_data))
    repeat, number = 10, 100
    r = t.repeat(repeat, number) 
    best, worst, avg = min(r), max(r), mean(r)
    best_mspc = best/number*1000
    worst_mspc = worst/number*1000
    avg_mspc = avg/number*1000
    
    if preambula is None:
        preambula = ""
    else:
        preambula = preambula+":: "
    print(f"{preambula}Stats from {number} loops, {repeat} times:")
    print(f"{preambula}mean  time: {avg_mspc:.3g} ms per call")
    print(f"{preambula}best  time: {best_mspc:.3g} ms per call")
    print(f"{preambula}worst time: {worst_mspc:.3g} ms per call")

    print(f"{preambula}Status: {last_validation_status()}")
    print(f"{preambula}Quick : {quick_check(img_data)}")
    print(f"{preambula}QuickStr: {quick_check_debug(img_data)}")


    print()

def profile_call(img_data):

    cProfile.run("validate(img_data)")

    print(f"Status: {last_validation_status()}")

if __name__ == "__main__":
    if len(sys.argv)==1:
        print(f"USAGE: python {sys.argv[0]} JPEG_FILENAME [JPEG_FILENAME [JPEG_FILENAME] [...]]")

    for filename in sys.argv[1:]:
        with open(filename, 'rb') as f:
            img_data = f.read()
        
            time_runs(img_data,preambula=filename)
            #profile_call(img_data)