from .base import MJPEGProxyBase

from queue import Empty
import time
from ..processing import PILPipeline,JPEGTranPipeline
from ..utils import Throttle

from ..frame import Frame

DEFAULT_THROTTLE_INTERVAL=0.02

class MJPEGProxy(MJPEGProxyBase):
    """
    this is a main proxy class for each of the clients
    """
    def __init__(self,flaskapp,name,config) -> None:
        
        super().__init__(flaskapp,name,config)

        self.register_image_generator("last",        self.last_generator,        title="Last known image",      hide=False)
        self.register_image_generator("valid",       self.last_valid_generator,  title="Last known valid image",hide=False)
        self.register_image_generator("next",        self.next_generator,        title="Next image",            hide=False)
        self.register_image_generator("raw",         self.raw_generator,         title="Next or last image",    hide=True)
        self.register_image_generator("rpi06",       self.rpi06_generator,       title="Data for rpi06",        hide=True)

        self.register_image_generator("autocontrast",self.autocontrast_generator,title="Autocontrast",          hide=True)
        self.register_image_generator("test",        self.test_generator,        title="Test processing",       hide=True)

        self._log.debug(f"Finished initializing proxy for {name}")


    def test_generator(self):
        """
        generate test image processing 
        """
        for frame in self.raw_generator():
            with PILPipeline(frame) as image:
                yield Frame(image.grayscale().autocontrast().bytes(),original=frame)

    def autocontrast_generator(self):
        """
        apply automatic contrast to the image
        """
        for frame in self.raw_generator():
            with PILPipeline(frame) as image:
                yield Frame(image.autocontrast().bytes(),original=frame)

    def next_generator(self):
        """
        next frame from the client
        """
        with self._mq.subscriber() as queue:
            while True:
                yield queue.get(timeout = self._mjpeg_frame_timeout)

    def last_generator(self,throttle=DEFAULT_THROTTLE_INTERVAL):
        """
        last known frame. yield rate will be throttled
        """
        while True:
            with Throttle(interval=throttle):
                yield self._cache.last_frame

    def last_valid_generator(self,throttle=DEFAULT_THROTTLE_INTERVAL):
        """
        last known valid frame. yield rate is throttled
        """
        while True:
            with Throttle(interval=throttle):
                yield self._cache.last_valid_frame
        #time.sleep(throttle)

    def raw_generator(self):
        """
        try to get a new frame, if fails - return last known
        """
        with self._mq.subscriber() as queue:
            while True:
                try:
                    yield queue.get(timeout = self._mjpeg_frame_timeout)
                except Empty:
                    yield self._cache.last_frame


    def rpi06_generator(self):
        """
        special preprocessor for rpi06 (down scaled image).
        This function will be removed in the future when proper processing pipeline will be implemented
        """
        for frame in self.raw_generator():
            with JPEGTranPipeline(frame) as image:
                yield Frame(image.fit(1280,1024).bytes(),original=frame)


