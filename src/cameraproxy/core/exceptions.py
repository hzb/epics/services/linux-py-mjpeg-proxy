#from builtins import Exception

class NetworkError(Exception):
    pass


class MJPEGError(Exception):
    pass

class TimeoutError(Exception):
    pass