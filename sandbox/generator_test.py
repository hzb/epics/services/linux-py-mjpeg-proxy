
def ff():
    ii=0
    try:
        while True:
            yield ii
            ii+=1
    except GeneratorExit:
        print("generator interrupted!")

def gg():
    ii=0
    while True:
        yield ii
        ii+=1

def hh():
    return 42

def call_once(generator):
    for result in generator():
        return result