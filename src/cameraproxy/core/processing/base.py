"""
processing.py - Image processing related code
"""

import gc

class Pipeline:
    def __init__(self,frame) -> None:
        pass

    def bytes(self):
        return b''

    def dispose(self):
        gc.collect()
        pass

    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.dispose()
