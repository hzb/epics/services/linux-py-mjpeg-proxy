from socket import timeout
import urllib
from urllib.error import URLError
from .utils import make_logger
from .exceptions import NetworkError
from collections.abc import MutableMapping

class HTTPCamera:
    """
    HTTPCamera implements basic / simplest interface for a HTTP MJPEG stream
    """
    def __init__(self,url_or_config,/,username=None, password=None, config=None) -> None:
        self._log = make_logger(__name__)
        _url = None
        if isinstance(url_or_config, MutableMapping):
            _url = url_or_config.get('url')
            if username is None:
                username = url_or_config.get('username')
            if password is None:
                password = url_or_config.get('password')
        else:
            _url = url_or_config
        self.url = _url
        self._stop = False
        self._open_timeout = 10 
        #self._reconnects = 0
        self._verbose = False
        if username is not None:
            self._add_user_pass(_url, username, password)

        self._config = config

    def mjpeg_stream(self):
        try:
            return urllib.request.urlopen(self.url,timeout=self._open_timeout)
        except URLError as e:
            raise NetworkError(e)

    def _add_user_pass(self, url, username, password, realm=None):
        """
        add basic authentication for the connection 

        based on the example from https://docs.python.org/3.9/howto/urllib2.html#id5
        """
        # create a password manager
        password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()

        # Add the real, username and password.
        password_mgr.add_password(None, url, username, password)
        handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
        # create "opener" (OpenerDirector instance)
        opener = urllib.request.build_opener(handler)

        # Install the opener.
        # Now all calls to urllib.request.urlopen use our opener.
        urllib.request.install_opener(opener)