from math import factorial
import jpegtran
from .base import Pipeline

class JPEGTranPipeline(Pipeline):
    def __init__(self, frame) -> None:
        self._data = jpegtran.JPEGImage(blob=frame)
        super().__init__(frame)
    
    def dispose(self):
        del self._data
        return super().dispose()

    def bytes(self):
        return self._data.as_blob()

    def fit(self, width, height):
        input_factor = self._data.width/self._data.height
        output_factor = width/height
        if input_factor>output_factor:
            height = round(self._data.height*width/self._data.width)
        else:
            width = round(self._data.width*height/self._data.height)
        self._data = self._data.downscale(width,height)
        return self