from PIL import Image

def check_jpeg(filename):
    with Image.open(filename) as im:
        result = True
        im.verify()
        print(f"Verification result for {filename}: {result}")
        return result

def verify_jpeg_image(file_path):
    try:
        with Image.open(file_path) as img:
            img.getdata()[0]
    except OSError:
        print(f"verify_jpeg_image False for {file_path}")
        return False
    print(f"verify_jpeg_image True for {file_path}")
    return True

check_jpeg("localonly/good.jpeg")
check_jpeg("localonly/bad.jpeg")
check_jpeg("localonly/bad2.jpeg")
check_jpeg("localonly/bad3.jpeg")

verify_jpeg_image("localonly/good.jpeg")
verify_jpeg_image("localonly/bad.jpeg")
verify_jpeg_image("localonly/bad2.jpeg")
verify_jpeg_image("localonly/bad3.jpeg")