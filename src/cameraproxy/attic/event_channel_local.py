"""

Simple event channel with callbacks implementation.

This is a fork with some local modifiations of 'event-channel 0.4.3' module by 'Author: mandrewcito'
PyPi version of the original module: https://pypi.org/project/event-channel/
Github of the original module: https://github.com/mandrewcito/event_channel

All modifications are by R. Ovsyannikov <ovsyannikov@helmholtz-berlin.de>

"""

import threading
import logging
from ..utils import make_logger


class EventChannel(object):
    def __init__(self):
        self._log = make_logger(__name__)
        self.__subscribers = {}

    def unsubscribe(self, event, callback):
        """
        unsubscribe a callback from selected event.
        Non-existing events or missing callbacks IS NOT AN ERROR
        """
        if event is not None or event != "" and event in self.__subscribers.keys():
            try: self.__subscribers[event].remove(callback)
            except: pass
        else:
            self._log.warning(f"Cant unsubscribe function '{event}' from event '{callback}'")

    def subscribe(self, event, callback):
        """
        add subscription callback to selected event
        """
        if not callable(callback): raise ValueError("callback must be callable")
        if event is None or event == "": raise ValueError("Event cant be empty")
        if event not in self.__subscribers.keys(): self.__subscribers[event] = set()
        self.__subscribers[event].add(callback)

    def publish(self, event, *args, **kwargs):
        """
        publish / push data for a selected event
        """
        if event in self.__subscribers.keys():
            for callback in self.__subscribers[event]:
                callback(*args, **kwargs)
        else:
            self._log.warning(f"Event {event} has no subscribers")


class ThreadedEventChannel(EventChannel):
    def __init__(self, blocking=True):
        self._blocking = blocking
        super(ThreadedEventChannel, self).__init__()

    def publish(self, event, *args, **kwargs):
        """
        publish / push data for a selected event.
        All callbacks will be executed in separate threads, one per each callback
        """
        threads = []

        if event not in self.__subscribers.keys():
            self._log.warning(f"Event {event} has no subscribers")
            return threads

        for callback in self.__subscribers[event]:
            threads.append(threading.Thread(
                target=callback,
                args=args,
                kwargs=kwargs
            ))

        for th in threads: th.start()

        if self.blocking: 
            for th in threads: th.join()

        return threads
